import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class CreateActivityInput {
  @Field()
  name:string

  @Field()
  description:string

  @Field()
  day:string

  @Field()
  location:number


}