
import { InputType,Field } from '@nestjs/graphql';

@InputType()
export class dayInput {
    @Field()
    day: string
}