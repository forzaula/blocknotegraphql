import { InputType,Field } from '@nestjs/graphql';

@InputType()
export class DateIntervalInput {
    @Field()
    startDay: string

    @Field()
    endDay: string

}