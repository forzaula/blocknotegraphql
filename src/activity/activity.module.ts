import { Module } from '@nestjs/common';
import { ActivityService } from './activity.service';
import { ActivityResolver } from './activity.resolver';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Activity} from "./entities/activity.entity";
import {AccountsModule} from "../accounts/accounts.module";
import {LocationsModule} from "../locations/locations.module";

@Module({
  imports:[TypeOrmModule.forFeature([Activity]),AccountsModule,LocationsModule],
  providers: [ActivityResolver, ActivityService]
})
export class ActivityModule {}
