import { HttpException, HttpStatus, Injectable} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository} from 'typeorm';
import { Activity } from './entities/activity.entity';
import {LocationsService} from "../locations/locations.service";
import {CreateActivityInput} from "./dto/create-activity.input";
import {JwtService} from "@nestjs/jwt";
import {UpdateActivityInput} from "./dto/update-activity.input";
import {dayInput} from "./dto/day.input";
import {DateIntervalInput} from "./dto/dateInterval.input";

@Injectable()
export class ActivityService {
  constructor(
      @InjectRepository(Activity)
      private readonly activityRepository: Repository<Activity>,
      private locationService: LocationsService,
      private readonly jwtService:JwtService
  ) {
  }


  async createActivity( dto: CreateActivityInput,token) {
    const hh = [dto.day]
    const ff = dto.location
    const location = await this.locationService.findLoc(ff)


    const gg=await this.locationService.findByLocationAndTime(ff)
    if (gg === undefined){
      const activity = new Activity()
      Object.assign(activity, dto)
      const date = hh[0]
      activity.location = location
      const currentUser=this.jwtService.verify(token)
      activity.account=currentUser

      return await this.activityRepository.save({...activity,day:new Date(date)})
    }
    const date = hh[0]
    const newDate = (new Date(date).toLocaleDateString()).toString()
    for (let x of gg ){
      if (newDate === x){
        throw new HttpException('на этот день занято', HttpStatus.NOT_FOUND)
      }
    }
    const activity = new Activity()
    Object.assign(activity, dto)
    const currentUser=this.jwtService.verify(token)

    activity.account=currentUser
    activity.location = location




    return await this.activityRepository.save({...activity,day:new Date(date)})
  }

  async deleteActivity(id: number,token) {
    const activity = await this.activityRepository.findOne(id)
    const currentUser=this.jwtService.verify(token)


    if (!activity) {
      throw new HttpException('activity does not exist', HttpStatus.NOT_FOUND)
    }
    if(activity.account.email!==currentUser.email){
      throw new HttpException('You are not author', HttpStatus.FORBIDDEN)

    }
    await this.activityRepository.delete({ id })
    return new HttpException('deleted', HttpStatus.OK)

  }

  async updateActivity(id: number, dto: UpdateActivityInput,token) {
    const activity = await this.activityRepository.findOne(id)
    const currentUser=this.jwtService.verify(token)
    const location = await this.locationService.findLoc(id)
    if (!activity) {
      throw new HttpException('activity does not exist', HttpStatus.NOT_FOUND)
    }
    if(activity.account.email!==currentUser.email){
      throw new HttpException('You are not author', HttpStatus.FORBIDDEN)
    }
    Object.assign(activity, dto)
    activity.account=currentUser
    activity.location = location
    return await this.activityRepository.save({...activity,day:new Date(dto.day)})
  }


  async findActivityByIntervalDate( dto: DateIntervalInput){
    const start = [dto.startDay]
    const end = [dto.endDay]
    const dates = await this.enumerateDaysBetweenDates(start[0],end[0])
    const data = await this.activityRepository.find()
    const dataActivity = []
    for (let x of data){
      const day = ((x.day).toLocaleDateString())
      const id = x.id
      for (let i of dates){
        if (i === day){
          dataActivity.push(id)
        }
      }
    }
    const dats = []
    for (let x of dataActivity){
      const resData = await this.activityRepository.findOne({id: x})
      console.log(resData)
      dats.push(resData)
    }
    return dats
  }

  async enumerateDaysBetweenDates(start, end){
    const arr = [];
    const dt = new Date(start);
    const ends = new Date(end);
    while (dt <= ends) {
      arr.push((new Date(dt)).toLocaleDateString());
      dt.setDate(dt.getDate() + 1);
    }
    return arr
  }
  async activitybylocation(id){
    const allActivity=await this.activityRepository.find()
    const locationId= await this.locationService.findLoc(id)
    const activity=[]
    for (let i of allActivity){
      if(i.location.id==locationId.id){
        activity.push(i)}
    }
    return activity
  }



  async avalaibleLocationByDate(dto: dayInput){
    const date = dto.day
    const newDate = new Date(date).toLocaleDateString()
    const allActivity = await this.activityRepository.find()
    const dataLocation = []
    const ola = await this.locationService.findAll()
    for ( let i of ola){
      dataLocation.push(i.id)
    }

    let dataDate = []
    const blackList = []
    for (let i of allActivity){
      if ( newDate === (i.day).toLocaleDateString()){
        for (let x of dataLocation){
          if (i.location.id === x){
            blackList.push(i.location.id)
            dataDate.push(i)
          }
        }
      }
    }
    let difference = dataLocation.filter(x => !blackList.includes(x)).concat(blackList.filter(x => !dataLocation.includes(x)));
    const veryWhitelist = []
    for ( let i of difference){
      let x = await this.locationService.findWhiteLocation(i)
      veryWhitelist.push(x)
    }

    return veryWhitelist
  }

}
