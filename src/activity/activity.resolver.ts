import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { ActivityService } from './activity.service';
import { Activity } from './entities/activity.entity';
import { CreateActivityInput } from './dto/create-activity.input';
import { UpdateActivityInput } from './dto/update-activity.input';
import {User} from "../decorators/user.decorator";
import {Account} from "../accounts/entities/account.entity";
import {UseGuards} from "@nestjs/common";
import {JwtAuthGuard} from "../accounts/jwt-auth.guard";
import {dayInput} from "./dto/day.input";
import {Location} from "../locations/entities/location.entity";
import {DateIntervalInput} from "./dto/dateInterval.input";

@Resolver(() => Activity)
export class ActivityResolver {
  constructor(private readonly activityService: ActivityService) {}

  @Mutation(() => Activity)
  @UseGuards(JwtAuthGuard)
  createActivity(@Args('createActivityInput') createActivityInput: CreateActivityInput,@User()currentUser: Account ) {
    return this.activityService.createActivity(createActivityInput,currentUser);
  }

  // @Query(() => [Activity], { name: 'activity' })
  // findAll() {
  //   return this.activityService.findAll();
  // }
  //
  // @Query(() => Activity, { name: 'activity' })
  // findOne(@Args('id', { type: () => Int }) id: number) {
  //   return this.activityService.findOne(id);
  // }
  //
  @Mutation(() => Activity)
  @UseGuards(JwtAuthGuard)
  updateActivity(@Args('updateActivityInput') updateActivityInput: UpdateActivityInput,@User()currentUser: Account ) {
    return this.activityService.updateActivity(updateActivityInput.id, updateActivityInput,currentUser);
  }
  //
  @Mutation(() => Activity)
  @UseGuards(JwtAuthGuard)
  removeActivity(@Args('id') id: number,@User()currentUser: Account) {
    return this.activityService.deleteActivity(id,currentUser);
  }
  @Query(()=>[Location], {name: 'availableLocationByDate'})
  availableLocationByDate(@Args('availableLocationByDate') day: dayInput){
    return this.activityService.avalaibleLocationByDate(day)
  }
  @Query(() => [Activity], { name: 'dates' })
  findActivityByIntervalDate(@Args('findActivityByIntervalDate') dateIntervalInput: DateIntervalInput ) {
    return this.activityService.findActivityByIntervalDate(dateIntervalInput);
  }
  @Query(() => [Activity], { name: 'locationactivity' })
  findByLocation(@Args('id',{type:()=>Int}) id: number) {
    return this.activityService.activitybylocation(id);
  }
}
