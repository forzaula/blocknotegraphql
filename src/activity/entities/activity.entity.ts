import {
  JoinColumn,
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne, BaseEntity,
} from 'typeorm';
import {Account} from "../../accounts/entities/account.entity";
import {Location} from "../../locations/entities/location.entity";
import {Field, ObjectType} from "@nestjs/graphql";


@Entity()
@ObjectType()
export class Activity{
  @PrimaryGeneratedColumn()
  @Field()
  id: number

  @Column()
  @Field()
  day: Date

  @Column({unique:true})
  @Field()
  name:string

  @Column()
  @Field()
  description:string

  @ManyToOne(type=>Location,location=>location.activities,{eager:true})
  @Field(type=>Location)
  @JoinColumn()
  location:Location

  @ManyToOne(()=>Account,account=>account=>account.activities,{eager:true})
  @Field(type=>Account)
  @JoinColumn()
  account:Account
}