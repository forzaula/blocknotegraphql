import {Resolver, Query, Mutation, Args} from '@nestjs/graphql';
import { LocationsService } from './locations.service';
import { Location } from './entities/location.entity';
import { CreateLocationInput } from './dto/create-location.input';
import {UpdateLocationInput} from "./dto/update-location.input";
import {Req, UseGuards} from "@nestjs/common";
import {Account} from "../accounts/entities/account.entity";
import {JwtAuthGuard} from "../accounts/jwt-auth.guard";
import {User} from "../decorators/user.decorator";
import {Activity} from "../activity/entities/activity.entity";

@Resolver(() => Location)
export class LocationsResolver {
  constructor(private readonly locationsService: LocationsService) {}

  @Mutation(() => Location)
  @UseGuards(JwtAuthGuard)
  createLocation(@Args('createLocationInput') createLocationInput: CreateLocationInput,@User()currentUser: Account ) {
    console.log(currentUser)
    return this.locationsService.createLocation(createLocationInput, currentUser);
  }

  @Query(() => [Location], { name: 'locations' })
  findAll() {
    return this.locationsService.findAll();
  }


  @Mutation(() => Location)
  updateLocation(@Args('updateLocationInput') updateLocationInput: UpdateLocationInput,@User()currentUser: Account) {
    return this.locationsService.updateLocation(updateLocationInput.id,updateLocationInput, currentUser);
  }

  @Mutation(() => Location)
  @UseGuards(JwtAuthGuard)
  removeLocation(@Args("id") id: number,@User()currentUser: Account ) {
    return this.locationsService.deleteLocation(id,currentUser);
  }
  // @Query(() => Location, { name: 'locationactivity' })
  // findByLocation(@Args('id') id: number) {
  //   return this.locationsService.findByLocation(id);
  // }
}
