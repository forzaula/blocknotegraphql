import { ObjectType, Field, Int } from '@nestjs/graphql';
import {BaseEntity, Column, Entity, JoinColumn, JoinTable, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Account} from "../../accounts/entities/account.entity";
import {Activity} from "../../activity/entities/activity.entity";


@Entity()
@ObjectType()
export class Location{

  @PrimaryGeneratedColumn()
  @Field()
  id: number;

  @Column()
  @Field()
  address: string

  @ManyToOne(type => Account, account => account.locations,{eager:true})
  @JoinTable()
  @Field(type =>Account, {nullable: true})
  account: Account;

  @OneToMany(type=>Activity,activity=>activity.location)
  @JoinColumn()
  @Field(type=>Activity)
  activities:Activity[]
}
